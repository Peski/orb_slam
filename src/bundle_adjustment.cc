// Copyright (c) 2018, ETH Zurich and UNC Chapel Hill.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//
//     * Neither the name of ETH Zurich and UNC Chapel Hill nor the names of
//       its contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Author: Johannes L. Schoenberger (jsch-at-demuc-dot-de)

#include "bundle_adjustment.h"

#include <map>
#include <iomanip>
#include <utility>

#ifdef OPENMP_ENABLED
#include <omp.h>
#endif

#include <ceres/ceres.h>
#include <ceres/rotation.h>

#include <colmap/util/logging.h>
#include <colmap/util/misc.h>
#include <colmap/util/threading.h>

/*
#include "base/camera_models.h"
#include "base/cost_functions.h"
#include "base/projection.h"
#include "util/misc.h"
#include "util/threading.h"
#include "util/timer.h"
*/

namespace ORB_SLAM {

// Standard bundle adjustment cost function for variable
// camera pose and point parameters.
class BundleAdjustmentCostFunction {
 public:
  explicit BundleAdjustmentCostFunction(const Eigen::Vector4d& pinhole_params, const Eigen::Vector2d& point2D)
      : fx_(pinhole_params(0)),
        fy_(pinhole_params(1)),
        cx_(pinhole_params(2)),
        cy_(pinhole_params(3)),
        observed_x_(point2D(0)),
        observed_y_(point2D(1)) {}

  static ceres::CostFunction* Create(const Eigen::Vector4d& pinhole_params, const Eigen::Vector2d& point2D) {
    return (new ceres::AutoDiffCostFunction<BundleAdjustmentCostFunction, 2, 4, 3, 3>(
              new BundleAdjustmentCostFunction(pinhole_params, point2D))
            );
  }

  template <typename T>
  bool operator()(const T* const qvec, const T* const tvec, const T* const point3D,
                  T* residuals) const {
    // Rotate and translate
    T projection[3];
    ceres::UnitQuaternionRotatePoint(qvec, point3D, projection);
    projection[0] += tvec[0];
    projection[1] += tvec[1];
    projection[2] += tvec[2];

    // Project to image plane
    projection[0] /= projection[2];
    projection[1] /= projection[2];

    // Transform to image coordinates (pinhole model)
    residuals[0] = T(fx_) * projection[0] + T(cx_);
    residuals[1] = T(fy_) * projection[1] + T(cy_);

    // Re-projection error
    residuals[0] -= T(observed_x_);
    residuals[1] -= T(observed_y_);

    return true;
  }

 private:
  const double fx_;
  const double fy_;
  const double cx_;
  const double cy_;
  const double observed_x_;
  const double observed_y_;
};

// Bundle adjustment cost function for fixed
// camera pose and variable point parameters.
class BundleAdjustmentConstantPoseCostFunction {
 public:
  BundleAdjustmentConstantPoseCostFunction(const Eigen::Vector4d& qvec,
                                           const Eigen::Vector3d& tvec,
                                           const Eigen::Vector4d& pinhole_params,
                                           const Eigen::Vector2d& point2D)
      : qw_(qvec(0)),
        qx_(qvec(1)),
        qy_(qvec(2)),
        qz_(qvec(3)),
        tx_(tvec(0)),
        ty_(tvec(1)),
        tz_(tvec(2)),
        fx_(pinhole_params(0)),
        fy_(pinhole_params(1)),
        cx_(pinhole_params(2)),
        cy_(pinhole_params(3)),
        observed_x_(point2D(0)),
        observed_y_(point2D(1)) {}

  static ceres::CostFunction* Create(const Eigen::Vector4d& qvec,
                                     const Eigen::Vector3d& tvec,
                                     const Eigen::Vector4d& pinhole_params,
                                     const Eigen::Vector2d& point2D) {
    return (new ceres::AutoDiffCostFunction<BundleAdjustmentConstantPoseCostFunction, 2, 3>(
              new BundleAdjustmentConstantPoseCostFunction(qvec, tvec, pinhole_params, point2D))
            );
  }

  template <typename T>
  bool operator()(const T* const point3D, T* residuals) const {
    const T qvec[4] = {T(qw_), T(qx_), T(qy_), T(qz_)};

    // Rotate and translate
    T projection[3];
    ceres::UnitQuaternionRotatePoint(qvec, point3D, projection);
    projection[0] += T(tx_);
    projection[1] += T(ty_);
    projection[2] += T(tz_);

    // Project to image plane
    projection[0] /= projection[2];
    projection[1] /= projection[2];

    // Transform to image coordinates (pinhole model)
    residuals[0] = T(fx_) * projection[0] + T(cx_);
    residuals[1] = T(fy_) * projection[1] + T(cy_);

    // Re-projection error
    residuals[0] -= T(observed_x_);
    residuals[1] -= T(observed_y_);

    return true;
  }

 private:
  const double qw_;
  const double qx_;
  const double qy_;
  const double qz_;
  const double tx_;
  const double ty_;
  const double tz_;
  const double fx_;
  const double fy_;
  const double cx_;
  const double cy_;
  const double observed_x_;
  const double observed_y_;
};

////////////////////////////////////////////////////////////////////////////////
// image_parameters_t
////////////////////////////////////////////////////////////////////////////////

image_parameters_t::image_parameters_t() {}

image_parameters_t::image_parameters_t(const Eigen::Vector4d& qvec, const Eigen::Vector3d& tvec)
  : qvec_(qvec), tvec_(tvec) {}

const Eigen::Vector4d& image_parameters_t::Qvec() const { return qvec_; }

Eigen::Vector4d& image_parameters_t::Qvec() { return qvec_; }

inline double image_parameters_t::Qvec(const size_t idx) const { return qvec_(idx); }

inline double& image_parameters_t::Qvec(const size_t idx) { return qvec_(idx); }

void image_parameters_t::SetQvec(const Eigen::Vector4d& qvec) { qvec_ = qvec; }

const Eigen::Vector3d& image_parameters_t::Tvec() const { return tvec_; }

Eigen::Vector3d& image_parameters_t::Tvec() { return tvec_; }

inline double image_parameters_t::Tvec(const size_t idx) const { return tvec_(idx); }

inline double& image_parameters_t::Tvec(const size_t idx) { return tvec_(idx); }

void image_parameters_t::SetTvec(const Eigen::Vector3d& tvec) { tvec_ = tvec; }

////////////////////////////////////////////////////////////////////////////////
// Reconstruction
////////////////////////////////////////////////////////////////////////////////

Reconstruction::Reconstruction() {}

size_t Reconstruction::NumImages() const { return images_.size(); }

size_t Reconstruction::NumPoints() const { return points_.size(); }

void Reconstruction::AddImage(image_t image, image_parameters_t parameters) {
  images_[image] = parameters;
}

void Reconstruction::AddPoint(point3D_t point, point3D_parameters_t parameters) {
  points_[point] = parameters;
}

bool Reconstruction::HasImage(image_t image) const {
  return (images_.find(image) != images_.end());
}

bool Reconstruction::HasPoint(point3D_t point) const {
  return (points_.find(point) != points_.end());
}

const image_parameters_t& Reconstruction::Image(image_t image) const {
  CHECK(HasImage(image));
  return images_.at(image);
}

image_parameters_t& Reconstruction::Image(image_t image) {
  CHECK(HasImage(image));
  return images_.at(image);
}

const std::unordered_map<image_t, image_parameters_t>& Reconstruction::Images() const {
  return images_;
}

const point3D_parameters_t& Reconstruction::Point(point3D_t point) const {
  CHECK(HasPoint(point));
  return points_.at(point);
}

point3D_parameters_t& Reconstruction::Point(point3D_t point) {
  CHECK(HasPoint(point));
  return points_.at(point);
}

const EIGEN_STL_UMAP(point3D_t, Eigen::Vector3d)& Reconstruction::Points() const {
  return points_;
}

////////////////////////////////////////////////////////////////////////////////
// BundleAdjustmentOptions
////////////////////////////////////////////////////////////////////////////////

ceres::LossFunction* BundleAdjustmentOptions::CreateLossFunction() const {
  ceres::LossFunction* loss_function = nullptr;
  switch (loss_function_type) {
    case LossFunctionType::TRIVIAL:
      loss_function = new ceres::TrivialLoss();
      break;
    case LossFunctionType::SOFT_L1:
      loss_function = new ceres::SoftLOneLoss(loss_function_scale);
      break;
    case LossFunctionType::CAUCHY:
      loss_function = new ceres::CauchyLoss(loss_function_scale);
      break;
  }
  CHECK_NOTNULL(loss_function);
  return loss_function;
}

bool BundleAdjustmentOptions::Check() const {
  CHECK_OPTION_GE(loss_function_scale, 0);
  return true;
}

////////////////////////////////////////////////////////////////////////////////
// BundleAdjustmentConfig
////////////////////////////////////////////////////////////////////////////////

BundleAdjustmentConfig::BundleAdjustmentConfig() {}

// size_t BundleAdjustmentConfig::NumImages() const { return image_ids_.size(); }

// size_t BundleAdjustmentConfig::NumPoints() const { return point3D_ids_.size(); }

/*
size_t BundleAdjustmentConfig::NumConstantCameras() const {
  return constant_camera_ids_.size();
}
*/

size_t BundleAdjustmentConfig::NumConstantPoses() const {
  return constant_poses_.size();
}

size_t BundleAdjustmentConfig::NumConstantTvecs() const {
  return constant_tvecs_.size();
}

/*
size_t BundleAdjustmentConfig::NumVariablePoints() const {
  return variable_point3D_ids_.size();
}
*/

size_t BundleAdjustmentConfig::NumConstantPoints() const {
  return constant_point3D_ids_.size();
}

/*
size_t BundleAdjustmentConfig::NumResiduals(
    const Reconstruction& reconstruction) const {
  // Count the number of observations for all added images.
  size_t num_observations = 0;
  for (const image_t image_id : image_ids_) {
    num_observations += reconstruction.Image(image_id).NumPoints3D();
  }

  // Count the number of observations for all added 3D points that are not
  // already added as part of the images above.

  auto NumObservationsForPoint = [this,
                                  &reconstruction](const point3D_t point3D_id) {
    size_t num_observations_for_point = 0;
    const auto& point3D = reconstruction.Point3D(point3D_id);
    for (const auto& track_el : point3D.Track().Elements()) {
      if (image_ids_.count(track_el.image_id) == 0) {
        num_observations_for_point += 1;
      }
    }
    return num_observations_for_point;
  };

  for (const auto point3D_id : variable_point3D_ids_) {
    num_observations += NumObservationsForPoint(point3D_id);
  }
  for (const auto point3D_id : constant_point3D_ids_) {
    num_observations += NumObservationsForPoint(point3D_id);
  }

  return 2 * num_observations;
}
*/

/*
void BundleAdjustmentConfig::AddImage(image_t image_id) {
  image_ids_.insert(image_id);
}

bool BundleAdjustmentConfig::HasImage(image_t image_id) const {
  return image_ids_.find(image_id) != image_ids_.end();
}

void BundleAdjustmentConfig::RemoveImage(image_t image_id) {
  image_ids_.erase(image_id);
}
*/

/*
void BundleAdjustmentConfig::SetConstantCamera(const camera_t camera_id) {
  constant_camera_ids_.insert(camera_id);
}

void BundleAdjustmentConfig::SetVariableCamera(const camera_t camera_id) {
  constant_camera_ids_.erase(camera_id);
}

bool BundleAdjustmentConfig::IsConstantCamera(const camera_t camera_id) const {
  return constant_camera_ids_.find(camera_id) != constant_camera_ids_.end();
}
*/

void BundleAdjustmentConfig::SetConstantPose(image_t image_id) {
  // CHECK(HasImage(image_id));
  CHECK(!HasConstantTvec(image_id));
  constant_poses_.insert(image_id);
}

/*
void BundleAdjustmentConfig::SetVariablePose(const image_t image_id) {
  constant_poses_.erase(image_id);
}
*/

bool BundleAdjustmentConfig::HasConstantPose(image_t image_id) const {
  return constant_poses_.find(image_id) != constant_poses_.end();
}

void BundleAdjustmentConfig::RemoveConstantPose(image_t image_id) {
  constant_poses_.erase(image_id);
}

void BundleAdjustmentConfig::SetConstantTvec(image_t image_id,
                                             const std::vector<int>& idxs) {
  CHECK_GT(idxs.size(), 0);
  CHECK_LE(idxs.size(), 3);
  // CHECK(HasImage(image_id));
  CHECK(!HasConstantPose(image_id));
  CHECK(!colmap::VectorContainsDuplicateValues(idxs))
      << "Tvec indices must not contain duplicates";
  constant_tvecs_.emplace(image_id, idxs);
}

bool BundleAdjustmentConfig::HasConstantTvec(const image_t image_id) const {
  return constant_tvecs_.find(image_id) != constant_tvecs_.end();
}

void BundleAdjustmentConfig::RemoveConstantTvec(image_t image_id) {
  constant_tvecs_.erase(image_id);
}

/*
void BundleAdjustmentConfig::AddVariablePoint(const point3D_t point3D_id) {
  CHECK(!HasConstantPoint(point3D_id));
  variable_point3D_ids_.insert(point3D_id);
}

void BundleAdjustmentConfig::AddConstantPoint(const point3D_t point3D_id) {
  CHECK(!HasVariablePoint(point3D_id));
  constant_point3D_ids_.insert(point3D_id);
}
*/

/*
void BundleAdjustmentConfig::AddPoint(point3D_t point3D_id) {
  point3D_ids_.insert(point3D_id);
}

bool BundleAdjustmentConfig::HasPoint(point3D_t point3D_id) const {
  return point3D_ids_.find(point3D_id) != point3D_ids_.end();
}

void BundleAdjustmentConfig::RemovePoint(point3D_t point3D_id) {
  point3D_ids_.erase(point3D_id);
}
*/

void BundleAdjustmentConfig::SetConstantPoint(point3D_t point3D_id) {
  // CHECK(HasPoint(point3D_id));
  constant_point3D_ids_.insert(point3D_id);
}

/*
bool BundleAdjustmentConfig::HasPoint(const point3D_t point3D_id) const {
  return HasVariablePoint(point3D_id) || HasConstantPoint(point3D_id);
}

bool BundleAdjustmentConfig::HasVariablePoint(
    const point3D_t point3D_id) const {
  return variable_point3D_ids_.find(point3D_id) != variable_point3D_ids_.end();
}
*/

bool BundleAdjustmentConfig::HasConstantPoint(point3D_t point3D_id) const {
  return constant_point3D_ids_.find(point3D_id) != constant_point3D_ids_.end();
}

/*
void BundleAdjustmentConfig::RemoveVariablePoint(const point3D_t point3D_id) {
  variable_point3D_ids_.erase(point3D_id);
}
*/

void BundleAdjustmentConfig::RemoveConstantPoint(point3D_t point3D_id) {
  constant_point3D_ids_.erase(point3D_id);
}

/*
const std::unordered_set<image_t>& BundleAdjustmentConfig::Images() const {
  return image_ids_;
}
*/

/*
const std::unordered_set<point3D_t>& BundleAdjustmentConfig::VariablePoints()
    const {
  return variable_point3D_ids_;
}
*/

/*
const std::unordered_set<point3D_t>& BundleAdjustmentConfig::Points() const {
  return point3D_ids_;
}
*/

const std::unordered_set<point3D_t>& BundleAdjustmentConfig::ConstantPoints() const {
  return constant_point3D_ids_;
}

const std::unordered_set<image_t>& BundleAdjustmentConfig::ConstantPoses() const {
  return constant_poses_;
}

const std::vector<int>& BundleAdjustmentConfig::ConstantTvec(image_t image_id) const {
  CHECK(HasConstantTvec(image_id));
  return constant_tvecs_.at(image_id);
}

const std::unordered_map<image_t, std::vector<int>>& BundleAdjustmentConfig::ConstantTvecs() const {
  return constant_tvecs_;
}

////////////////////////////////////////////////////////////////////////////////
// BundleAdjuster
////////////////////////////////////////////////////////////////////////////////

BundleAdjuster::BundleAdjuster(const BundleAdjustmentOptions& options,
                               const BundleAdjustmentConfig& config)
    : options_(options), config_(config) {
  CHECK(options_.Check());
}

bool BundleAdjuster::Solve(Reconstruction* reconstruction) {
  CHECK_NOTNULL(reconstruction);
  CHECK(!problem_) << "Cannot use the same BundleAdjuster multiple times";

  problem_.reset(new ceres::Problem());

  ceres::LossFunction* loss_function = options_.CreateLossFunction();
  SetUp(reconstruction, loss_function);

  if (problem_->NumResiduals() == 0) {
    return false;
  }

  ceres::Solver::Options solver_options = options_.solver_options;

  // Empirical choice.
  const size_t kMaxNumImagesDirectDenseSolver = 50;
  const size_t kMaxNumImagesDirectSparseSolver = 1000;
  const size_t num_images = reconstruction->NumImages();
  if (num_images <= kMaxNumImagesDirectDenseSolver) {
    solver_options.linear_solver_type = ceres::DENSE_SCHUR;
  } else if (num_images <= kMaxNumImagesDirectSparseSolver) {
    solver_options.linear_solver_type = ceres::SPARSE_SCHUR;
  } else {  // Indirect sparse (preconditioned CG) solver.
    solver_options.linear_solver_type = ceres::ITERATIVE_SCHUR;
    solver_options.preconditioner_type = ceres::SCHUR_JACOBI;
  }

  solver_options.num_threads =
      colmap::GetEffectiveNumThreads(solver_options.num_threads);
#if CERES_VERSION_MAJOR < 2
  solver_options.num_linear_solver_threads =
      colmap::GetEffectiveNumThreads(solver_options.num_linear_solver_threads);
#endif  // CERES_VERSION_MAJOR

  std::string solver_error;
  CHECK(solver_options.IsValid(&solver_error)) << solver_error;

  ceres::Solve(solver_options, problem_.get(), &summary_);

  if (solver_options.minimizer_progress_to_stdout) {
    std::cout << std::endl;
  }

  if (options_.print_summary) {
    colmap::PrintHeading2("Bundle adjustment report");
    PrintSolverSummary(summary_);
  }

  TearDown(reconstruction);

  return true;
}

const ceres::Solver::Summary& BundleAdjuster::Summary() const {
  return summary_;
}

void BundleAdjuster::SetUp(Reconstruction* reconstruction,
                           ceres::LossFunction* loss_function) {
  for (std::pair<point3D_t, Eigen::Vector3d> p : reconstruction->Points()) {
    MapPoint* pMP = p.first;
    if(pMP->isBad())
      continue;

    for (std::pair<KeyFrame*, size_t> m : pMP->GetObservations()) {
      KeyFrame* pKF = m.first;
      if(pKF->isBad())
        continue;

      cv::KeyPoint kpUn = pKF->GetKeyPointUn(m.second);
      Eigen::Vector2d obs(kpUn.pt.x, kpUn.pt.y);

      Eigen::Vector4d camera_params(pKF->fx, pKF->fy, pKF->cx, pKF->cy);

      // Cost function
      ceres::CostFunction* cost_function = nullptr;

      // TODO Constant pose, Weight, Huber
      CHECK(reconstruction->HasImage(pKF));

      double* qvec_data = reconstruction->Image(pKF).Qvec().data();
      double* tvec_data = reconstruction->Image(pKF).Tvec().data();
      double* xyz_data = reconstruction->Point(pMP).data();

      if (config_.HasConstantPose(pKF)) {
        cost_function = BundleAdjustmentConstantPoseCostFunction::Create(
          reconstruction->Image(pKF).Qvec(),
          reconstruction->Image(pKF).Tvec(),
          camera_params, obs);

        // Add parameter block
        problem_->AddResidualBlock(cost_function, loss_function, xyz_data);
      } else {
        cost_function = BundleAdjustmentCostFunction::Create(camera_params, obs);

        // Add parameter block
        CHECK(cost_function);
        problem_->AddResidualBlock(cost_function, loss_function, qvec_data,
                                   tvec_data, xyz_data);
      }

      // TODO Parameterize Point and Image according to config_
    }
  }
}

void BundleAdjuster::TearDown(Reconstruction*) {
  // Nothing to do
}

/*
void BundleAdjuster::AddImageToProblem(image_t image_id,
                                       Reconstruction* reconstruction,
                                       ceres::LossFunction* loss_function) {
  image_parameters_t& image = reconstruction->Image(image_id);
  // Camera& camera = reconstruction->Camera(image.CameraId());

  // CostFunction assumes unit quaternions.
  image.Qvec() = colmap::NormalizeQuaternion(image.Qvec());

  double* qvec_data = image.Qvec().data();
  double* tvec_data = image.Tvec().data();
  double* camera_params_data = camera.ParamsData();

  const bool constant_pose = config_.HasConstantPose(image_id);

  // Add residuals to bundle adjustment problem.
  size_t num_observations = 0;
  for (const Point2D& point2D : image.Points2D()) {
    if (!point2D.HasPoint3D()) {
      continue;
    }

    num_observations += 1;
    point3D_num_observations_[point2D.Point3DId()] += 1;

    Point3D& point3D = reconstruction->Point3D(point2D.Point3DId());
    assert(point3D.Track().Length() > 1);

    ceres::CostFunction* cost_function = nullptr;

    if (constant_pose) {
      switch (camera.ModelId()) {
#define CAMERA_MODEL_CASE(CameraModel)                                 \
  case CameraModel::kModelId:                                          \
    cost_function =                                                    \
        BundleAdjustmentConstantPoseCostFunction<CameraModel>::Create( \
            image.Qvec(), image.Tvec(), point2D.XY());                 \
    break;

        CAMERA_MODEL_SWITCH_CASES

#undef CAMERA_MODEL_CASE
      }

      problem_->AddResidualBlock(cost_function, loss_function,
                                 point3D.XYZ().data(), camera_params_data);
    } else {
      switch (camera.ModelId()) {
#define CAMERA_MODEL_CASE(CameraModel)                                   \
  case CameraModel::kModelId:                                            \
    cost_function =                                                      \
        BundleAdjustmentCostFunction<CameraModel>::Create(point2D.XY()); \
    break;

        CAMERA_MODEL_SWITCH_CASES

#undef CAMERA_MODEL_CASE
      }

      problem_->AddResidualBlock(cost_function, loss_function, qvec_data,
                                 tvec_data, point3D.XYZ().data(),
                                 camera_params_data);
    }
  }

  if (num_observations > 0) {
    camera_ids_.insert(image.CameraId());

    // Set pose parameterization.
    if (!constant_pose) {
      ceres::LocalParameterization* quaternion_parameterization =
          new ceres::QuaternionParameterization;
      problem_->SetParameterization(qvec_data, quaternion_parameterization);
      if (config_.HasConstantTvec(image_id)) {
        const std::vector<int>& constant_tvec_idxs =
            config_.ConstantTvec(image_id);
        ceres::SubsetParameterization* tvec_parameterization =
            new ceres::SubsetParameterization(3, constant_tvec_idxs);
        problem_->SetParameterization(tvec_data, tvec_parameterization);
      }
    }
  }
}

void BundleAdjuster::AddPointToProblem(point3D_t point3D_id,
                                       Reconstruction* reconstruction,
                                       ceres::LossFunction* loss_function) {
  Point3D& point3D = reconstruction->Point3D(point3D_id);

  // Is 3D point already fully contained in the problem? I.e. its entire track
  // is contained in `variable_image_ids`, `constant_image_ids`,
  // `constant_x_image_ids`.
  if (point3D_num_observations_[point3D_id] == point3D.Track().Length()) {
    return;
  }

  for (const auto& track_el : point3D.Track().Elements()) {
    // Skip observations that were already added in `FillImages`.
    if (config_.HasImage(track_el.image_id)) {
      continue;
    }

    point3D_num_observations_[point3D_id] += 1;

    Image& image = reconstruction->Image(track_el.image_id);
    Camera& camera = reconstruction->Camera(image.CameraId());
    const Point2D& point2D = image.Point2D(track_el.point2D_idx);

    // We do not want to refine the camera of images that are not
    // part of `constant_image_ids_`, `constant_image_ids_`,
    // `constant_x_image_ids_`.
    if (camera_ids_.count(image.CameraId()) == 0) {
      camera_ids_.insert(image.CameraId());
      config_.SetConstantCamera(image.CameraId());
    }

    ceres::CostFunction* cost_function = nullptr;

    switch (camera.ModelId()) {
#define CAMERA_MODEL_CASE(CameraModel)                                 \
  case CameraModel::kModelId:                                          \
    cost_function =                                                    \
        BundleAdjustmentConstantPoseCostFunction<CameraModel>::Create( \
            image.Qvec(), image.Tvec(), point2D.XY());                 \
    break;

      CAMERA_MODEL_SWITCH_CASES

#undef CAMERA_MODEL_CASE
    }
    problem_->AddResidualBlock(cost_function, loss_function,
                               point3D.XYZ().data(), camera.ParamsData());
  }
}
*/

/*
void BundleAdjuster::ParameterizeCameras(Reconstruction* reconstruction) {
  const bool constant_camera = !options_.refine_focal_length &&
                               !options_.refine_principal_point &&
                               !options_.refine_extra_params;
  for (const camera_t camera_id : camera_ids_) {
    Camera& camera = reconstruction->Camera(camera_id);

    if (constant_camera || config_.IsConstantCamera(camera_id)) {
      problem_->SetParameterBlockConstant(camera.ParamsData());
      continue;
    } else {
      std::vector<int> const_camera_params;

      if (!options_.refine_focal_length) {
        const std::vector<size_t>& params_idxs = camera.FocalLengthIdxs();
        const_camera_params.insert(const_camera_params.end(),
                                   params_idxs.begin(), params_idxs.end());
      }
      if (!options_.refine_principal_point) {
        const std::vector<size_t>& params_idxs = camera.PrincipalPointIdxs();
        const_camera_params.insert(const_camera_params.end(),
                                   params_idxs.begin(), params_idxs.end());
      }
      if (!options_.refine_extra_params) {
        const std::vector<size_t>& params_idxs = camera.ExtraParamsIdxs();
        const_camera_params.insert(const_camera_params.end(),
                                   params_idxs.begin(), params_idxs.end());
      }

      if (const_camera_params.size() > 0) {
        ceres::SubsetParameterization* camera_params_parameterization =
            new ceres::SubsetParameterization(
                static_cast<int>(camera.NumParams()), const_camera_params);
        problem_->SetParameterization(camera.ParamsData(),
                                      camera_params_parameterization);
      }
    }
  }
}
*/

/*
void BundleAdjuster::ParameterizePoints(Reconstruction* reconstruction) {
  for (const auto elem : point3D_num_observations_) {
    Point3D& point3D = reconstruction->Point3D(elem.first);
    if (point3D.Track().Length() > elem.second) {
      problem_->SetParameterBlockConstant(point3D.XYZ().data());
    }
  }

  for (const point3D_t point3D_id : config_.ConstantPoints()) {
    Point3D& point3D = reconstruction->Point3D(point3D_id);
    problem_->SetParameterBlockConstant(point3D.XYZ().data());
  }
}
*/

////////////////////////////////////////////////////////////////////////////////

void PrintSolverSummary(const ceres::Solver::Summary& summary) {
  std::cout << std::right << std::setw(16) << "Residuals : ";
  std::cout << std::left << summary.num_residuals_reduced << std::endl;

  std::cout << std::right << std::setw(16) << "Parameters : ";
  std::cout << std::left << summary.num_effective_parameters_reduced
            << std::endl;

  std::cout << std::right << std::setw(16) << "Iterations : ";
  std::cout << std::left
            << summary.num_successful_steps + summary.num_unsuccessful_steps
            << std::endl;

  std::cout << std::right << std::setw(16) << "Time : ";
  std::cout << std::left << summary.total_time_in_seconds << " [s]"
            << std::endl;

  std::cout << std::right << std::setw(16) << "Initial cost : ";
  std::cout << std::right << std::setprecision(6)
            << std::sqrt(summary.initial_cost / summary.num_residuals_reduced)
            << " [px]" << std::endl;

  std::cout << std::right << std::setw(16) << "Final cost : ";
  std::cout << std::right << std::setprecision(6)
            << std::sqrt(summary.final_cost / summary.num_residuals_reduced)
            << " [px]" << std::endl;

  std::cout << std::right << std::setw(16) << "Termination : ";

  std::string termination = "";

  switch (summary.termination_type) {
    case ceres::CONVERGENCE:
      termination = "Convergence";
      break;
    case ceres::NO_CONVERGENCE:
      termination = "No convergence";
      break;
    case ceres::FAILURE:
      termination = "Failure";
      break;
    case ceres::USER_SUCCESS:
      termination = "User success";
      break;
    case ceres::USER_FAILURE:
      termination = "User failure";
      break;
    default:
      termination = "Unknown";
      break;
  }

  std::cout << std::right << termination << std::endl;
  std::cout << std::endl;
}

////////////////////////////////////////////////////////////////////////////////

}  // namespace ORB_SLAM

